using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RagdollController : MonoBehaviour
{

    private Rigidbody[] m_Bones;
    private Animator m_Animator;
    [SerializeField] private EnemieAgent _enemieAgent;

    private void Awake()
    {
        // m_Animator = GetComponent<Animator>();
        m_Bones = _enemieAgent.GetComponentsInChildren<Rigidbody>();
        Activate(false);

        foreach (Rigidbody bone in m_Bones)
            if(bone.TryGetComponent<IDamageable>(out IDamageable damageable))
                damageable.OnDamage += ReceiveDamage;
    }

    public void Activate(bool state)
    {
        foreach (Rigidbody bone in m_Bones)
            bone.isKinematic = !state;
        // m_Animator.enabled = !state;
    }

    private void ReceiveDamage(int damage)
    {
       _enemieAgent.OnHit(damage);
    }

    public void Die()
    {
        foreach (Rigidbody bone in m_Bones)
            if (bone.TryGetComponent<IDamageable>(out IDamageable damageable))
                damageable.OnDamage -= ReceiveDamage;

        Activate(true);
    }
}
