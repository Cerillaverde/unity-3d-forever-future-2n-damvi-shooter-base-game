using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemieAgent : MonoBehaviour
{
    [SerializeField] private LayerMask layerMask;
    [SerializeField] private LayerMask shootMask;
    private UnityEngine.AI.NavMeshAgent agent;

    [SerializeField] private float hpMax;
    [SerializeField] private float rangeDetection;
    [SerializeField] private float rangeAttack;
    private float hpActual;
    private float moveSpeed;
    private float damage;

    private bool playerTraked;
    private Collider[] playerCollider;
    private Vector3 originPosition;
    private Vector3 nextDestination;
    private bool waitingForNextPath;
    private bool canAttack;

    protected enum SwitchMachineStates
    {
        NONE,
        IDLE,
        WALK,
        ATTACK,
        RECIVEHIT,
        DIE
    };

    protected SwitchMachineStates currentState;
    protected SwitchMachineStates lastState;


    void Start()
    {
        moveSpeed = Random.Range(1.5f, 5f);
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        canAttack = true;
        originPosition = transform.position;
        hpActual = hpMax;
        InitState(SwitchMachineStates.IDLE);
    }

    void Update()
    {
        playerCollider = Physics.OverlapSphere(transform.position, rangeDetection, layerMask);
        UpdateState();

        if (playerCollider.Length > 0)
            TrackedPlayer(playerCollider[0]);

        if (playerCollider.Length == 0 && playerTraked)
            StartCoroutine(StayOnSite());
        else if (agent.remainingDistance < 1.2f && !waitingForNextPath && playerCollider.Length == 0)
            StartCoroutine(CooldownToNextPoint(5f));
    }

    public void OnHit(int damage)
    {
        hpActual -= damage;
        
        if (hpActual > 0)
            ChangeState(SwitchMachineStates.RECIVEHIT);
        else
            ChangeState(SwitchMachineStates.DIE);
    }
    
    IEnumerator StayOnSite()
    {
        agent.speed = 0;
        waitingForNextPath = true;
        yield return new WaitForSeconds(1.5f);
        agent.SetDestination(originPosition);
    }

    void TrackedPlayer(Collider playerCol)
    {
        StopCoroutine(StayOnSite());
        agent.SetDestination(playerCol.gameObject.transform.position);
        if (agent.remainingDistance < rangeAttack)
        {
            agent.speed = 0;
            if (canAttack)
            {
                ShootToPlayer(playerCol);
                // ChangeState(SwitchMachineStates.ATTACK);
            }
        }
        else
            agent.speed = moveSpeed;
    }
    
    private void ShootToPlayer(Collider playerCol)
    {
        canAttack = false;

        Vector3 accuracy = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0);
        
        RaycastHit hit;
        if (Physics.Raycast(transform.position, playerCol.transform.position - transform.position + accuracy, out hit, 15f, shootMask))
        {
            Debug.DrawLine(transform.position, hit.point, Color.green, 2f);

            if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                target.Damage(5);

        }
        StartCoroutine(ShootCoolDown(0.8f));
    }
    
    IEnumerator ShootCoolDown(float time)
    {
        yield return new WaitForSeconds(time);
        canAttack = true;
    }

    IEnumerator CooldownToNextPoint(float time)
    {
        agent.speed = 0;
        waitingForNextPath = true;
        yield return new WaitForSeconds(time);
        GotoNextPoint();
    }

    void GotoNextPoint()
    {
        float random = Random.Range(4, 15);
        switch (Random.Range(0, 4))
        {
            case 0:
                nextDestination = new Vector3(transform.position.x + random, transform.position.y,
                    transform.position.z);
                agent.SetDestination(nextDestination);
                break;
            case 1:
                nextDestination = new Vector3(transform.position.x - random, transform.position.y,
                    transform.position.z);
                agent.SetDestination(nextDestination);
                break;
            case 2:
                nextDestination =
                    new Vector3(transform.position.x, transform.position.y, transform.position.z + random);
                agent.SetDestination(nextDestination);
                break;
            case 3:
                nextDestination = new Vector3(transform.position.x, transform.position.y,
                    (transform.position.z - random));
                agent.SetDestination(nextDestination);
                break;
        }

        agent.speed = moveSpeed;
        waitingForNextPath = false;
    }

    #region SwitchMachineStates

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == currentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        this.currentState = currentState;
        switch (this.currentState)
        {
            case SwitchMachineStates.IDLE:
                agent.speed = 0;

                break;


            case SwitchMachineStates.WALK:
                break;


            case SwitchMachineStates.ATTACK:
                agent.speed = 0;
                break;

            case SwitchMachineStates.RECIVEHIT:
                agent.speed = 0;
                StartCoroutine(StunTime());
                break;

            case SwitchMachineStates.DIE:
                Destroy(gameObject);
                break;

            default:
                break;
        }
    }

    IEnumerator StunTime()
    {
        yield return new WaitForSeconds(1f);
        ChangeState(lastState);
    }

    private void ExitState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;

            case SwitchMachineStates.ATTACK:
                break;

            case SwitchMachineStates.RECIVEHIT:
                break;

            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (currentState)
        {
            case SwitchMachineStates.IDLE:

                if (agent.speed != 0)
                    ChangeState(SwitchMachineStates.WALK);

                lastState = currentState;
                break;

            case SwitchMachineStates.WALK:

                if (agent.speed == 0)
                    ChangeState(SwitchMachineStates.IDLE);

                lastState = currentState;
                break;

            case SwitchMachineStates.ATTACK:
                break;
            case SwitchMachineStates.RECIVEHIT:
                break;
            case SwitchMachineStates.DIE:
                break;

            default:
                break;
        }
    }

    #endregion
}