
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class BasePlayerControl : MonoBehaviour
{
    //***Control of player***
    [SerializeField] private float speed;
    [SerializeField] private float sensitivity;
    [SerializeField] private float jumpHeight;
    [SerializeField] private float rangeOfAccuracy;

    private float oldSpeed;
    private float gravity = 7f;
    private int hpMax = 100;
    private int hpActual;
    private int Gun1MaxAmmo = 7;
    private int Gun1ActualAmmo;
    private int Gun2MaxAmmo = 20;
    private int Gun2ActualAmmo;
    private int damage;


    private CharacterController characterController;
    private Vector3 moveDirection;
    private PlayerInput playerInput;
    private Vector2 movement;
    private Vector3 recoil;

    //***Control of camera***
    private Vector3 camRotation;
    [Range(-45, -15)] [SerializeField] private int minAngle = -30;
    [Range(30, 80)] [SerializeField] private int maxAngle = 45;
    [SerializeField] private LayerMask shootMask;
    [SerializeField] private Camera aerialCamera;

    private Transform _camera;
    private bool holded;
    private bool canShoot = true;

    private int weaponActive = 1;

    private void Awake()
    {
        _camera = Camera.main.transform;
        characterController = GetComponent<CharacterController>();
        Cursor.lockState = CursorLockMode.Locked;
        playerInput = GetComponent<PlayerInput>();
    }

    void Start()
    {
        hpActual = hpMax;
        oldSpeed = speed;
        if (this.TryGetComponent<IDamageable>(out IDamageable damageable))
            damageable.OnDamage += Hit;

        Gun1ActualAmmo = Gun1MaxAmmo;
        Gun2ActualAmmo = Gun2MaxAmmo;
        
        GameManager.Instance.WeaponActive(1, Gun1MaxAmmo);
        GameManager.Instance.UseAmmo(Gun1ActualAmmo);
        damage = 10;
        GameManager.Instance.LifeUpdate(hpMax);
    }

    private void Update()
    {
        movement = playerInput.actions["Move"].ReadValue<Vector2>();
    }

    void FixedUpdate()
    {
        Move();
        Rotate();
    }

    private void Rotate()
    {
        transform.Rotate(Vector3.up * sensitivity * Input.GetAxis("Mouse X"));

        camRotation.x -= Input.GetAxis("Mouse Y") * sensitivity;
        camRotation.x = Mathf.Clamp(camRotation.x, minAngle, maxAngle);

        camRotation += recoil;

        _camera.transform.localEulerAngles = camRotation;
    }

    private void Move()
    {
        if (characterController.isGrounded)
        {
            moveDirection = new Vector3(movement.x, 0, movement.y);
            moveDirection = transform.TransformDirection(moveDirection);
        }

        if (characterController.isGrounded && Input.GetKey(KeyCode.Space))
            moveDirection.y = jumpHeight;

        moveDirection.y -= gravity * Time.deltaTime;
        characterController.Move(moveDirection * speed * Time.deltaTime);
    }

    private void Hit(int damage)
    {
        hpActual -= damage;
        GameManager.Instance.LifeUpdate(hpActual);
        if (hpActual <= 0)
        {
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene(1);
        }
    }


    public void ActiveWeapon1(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            weaponActive = 1;
            damage = 10;
            GameManager.Instance.WeaponActive(1, Gun1MaxAmmo);
            GameManager.Instance.UseAmmo(Gun1ActualAmmo);
        }
    }

    public void ActiveWeapon2(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            weaponActive = 2;
            damage = 5;
            GameManager.Instance.WeaponActive(2, Gun2MaxAmmo);
            GameManager.Instance.UseAmmo(Gun2ActualAmmo);
        }
    }

    public void ShootAction(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            holded = true;
            Shoot();
        }

        if (callbackContext.canceled)
        {
            recoil = Vector3.zero;
            holded = false;
        }
    }

    public void ReloadAmmo(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            if (weaponActive == 1)
                StartCoroutine(ReloadAmmo(1f));
            if (weaponActive == 2)
                StartCoroutine(ReloadAmmo(2f));
        }
    }

    private void Shoot()
    {
        if (weaponActive == 1 && Gun1ActualAmmo > 0 || weaponActive == 2 && Gun2ActualAmmo > 0)
        {
            RaycastHit hit;
            Vector3 accuracy = new Vector3(-(Random.Range(0, rangeOfAccuracy)),
                Random.Range(-rangeOfAccuracy, rangeOfAccuracy), 0);

            if (characterController.velocity == Vector3.zero)
            {
                if (Physics.Raycast(_camera.position, _camera.forward, out hit, 40f, shootMask))
                {
                    Debug.DrawLine(_camera.position, hit.point, Color.red, 2f);
                    HitControl(hit);
                }
            }
            else
            {
                if (Physics.Raycast(_camera.position, _camera.forward + (accuracy * 2), out hit, 40f, shootMask))
                {
                    Debug.DrawLine(_camera.position, hit.point, Color.red, 2f);
                    HitControl(hit);
                }
            }

            if (weaponActive == 1 && canShoot)
            {
                recoil = accuracy / 2;
                Gun1ActualAmmo--;
                GameManager.Instance.UseAmmo(Gun1ActualAmmo);
                StartCoroutine(StopRecoil());
                StartCoroutine(ShootCooldown(0.4f));
            }

            if (weaponActive == 2 && canShoot)
            {
                recoil = accuracy;
                Gun2ActualAmmo--;
                GameManager.Instance.UseAmmo(Gun2ActualAmmo);
                StartCoroutine(ShootCooldown(0.1f));
            }
            
            
        }
        else if (weaponActive == 1 && Gun1ActualAmmo <= 0 || weaponActive == 2 && Gun2ActualAmmo <= 0)
        {
            recoil = Vector3.zero;
            if (weaponActive == 1)
                StartCoroutine(ReloadAmmo(1f));
            if (weaponActive == 2)
                StartCoroutine(ReloadAmmo(2f));
        }
    }

    IEnumerator StopRecoil()
    {
        yield return new WaitForSeconds(0.1f);
        recoil = Vector3.zero;
    }

    IEnumerator ReloadAmmo(float time)
    {
        GameManager.Instance.AmmoReload(true);
        yield return new WaitForSeconds(time);
        if (weaponActive == 1)
            Gun1ActualAmmo = Gun1MaxAmmo;
        else
            Gun2ActualAmmo = Gun2MaxAmmo;
        GameManager.Instance.AmmoReload(false);
        GameManager.Instance.UseAmmo(weaponActive == 1 ? Gun1MaxAmmo : Gun2MaxAmmo);
        if (holded)
            Shoot();
    }

    private void HitControl(RaycastHit raycastHit)
    {
        if (raycastHit.collider.TryGetComponent<IDamageable>(out IDamageable target))
        {
            Debug.Log(raycastHit.collider.gameObject.tag);
            if (raycastHit.collider.gameObject.CompareTag("Head"))
                target.Damage(damage * 2);
            else if (raycastHit.collider.gameObject.CompareTag("Chest"))
                target.Damage(damage);
            else
                target.Damage(damage / 5);
        }
    }

    IEnumerator ShootCooldown(float time)
    {
        canShoot = false;
        yield return new WaitForSeconds(time);
        canShoot = true;
        if (holded)
            Shoot();
    }

    public void AerialCamera(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            GameManager.Instance.AerialCamera(true);
            aerialCamera.gameObject.SetActive(true);
            _camera.gameObject.SetActive(false);
        }

        if (callbackContext.canceled)
        {
            GameManager.Instance.AerialCamera(false);
            _camera.gameObject.SetActive(true);
            aerialCamera.gameObject.SetActive(false);
        }
    }

    public void Run(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            speed *= 2f;
        }

        if (callbackContext.canceled)
        {
            speed = oldSpeed;
        }
    }
}