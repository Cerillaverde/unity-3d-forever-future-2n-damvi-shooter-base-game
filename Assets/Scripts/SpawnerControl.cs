using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpawnerControl : MonoBehaviour
{

    [SerializeField] private float timeToSpawn;
    [SerializeField] private GameObject enemie;
    
    void Start()
    {
        StartCoroutine(SpawnEnemies(timeToSpawn));
    }

    IEnumerator SpawnEnemies(float time)
    {
        while (true)
        {
            GameObject newMob = Instantiate(enemie);
            NavMeshAgent mobNav = newMob.GetComponent<NavMeshAgent>();
            mobNav.Warp(transform.position);
            yield return new WaitForSeconds(time);
        }
    }

}
