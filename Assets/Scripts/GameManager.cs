using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;

    [SerializeField] private GameObject mirilla;
    [SerializeField] private TMP_Text hpActual;
    [SerializeField] private GameObject arma1;
    [SerializeField] private GameObject arma2;
    [SerializeField] private GameObject hit;
    [SerializeField] private TMP_Text ammo;
    [SerializeField] private TMP_Text ammoMax;
    [SerializeField] private GameObject ammoReload;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this);
    }

    void Start()
    {
        arma1.SetActive(true);
        arma2.SetActive(false);
    }

    public void AmmoReload(bool active)
    {
        ammoReload.SetActive(active);
    }

    public void UseAmmo(int actualAmmo)
    {
        ammo.text = actualAmmo + "";
    }

    public void LifeUpdate(int hp)
    {
        hpActual.text = hp + "";
        if (hp < 20)
            hpActual.color = Color.red;
        else
            hpActual.color = Color.white;
        
        hit.SetActive(true);
        StartCoroutine(DesactivateHitPanel());
    }

    IEnumerator DesactivateHitPanel()
    {
        yield return new WaitForSeconds(0.2f);
        hit.SetActive(false);
    }

    public void WeaponActive(int weapon, int maxAmmo)
    {
        if (weapon == 1)
        {
            arma1.SetActive(true);
            arma2.SetActive(false);
        }

        if (weapon == 2)
        {
            arma2.SetActive(true);
            arma1.SetActive(false);
        }
        
        ammoMax.text = maxAmmo + "";
    }

    public void AerialCamera(bool activate)
    {
        mirilla.SetActive(!activate);
    }
}